import { Component, ViewChild } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController, App, FabContainer, ItemSliding, List, ModalController, NavController, ToastController, LoadingController, Refresher, Slides } from 'ionic-angular';

/*
  To learn how to use third party libs in an
  Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
*/
// import moment from 'moment';

import { ConferenceData } from '../../providers/conference-data';
import { ProductsData } from '../../providers/products-data';
import { UserData } from '../../providers/user-data';

import { SessionDetailPage } from '../session-detail/session-detail';
import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { SpeakerDetailPage } from '../speaker-detail/speaker-detail';


@Component({
  
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  providers: [ProductsData]


  
})
export class SchedulePage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  @ViewChild('scheduleList', { read: List }) scheduleList: List;

  dayIndex                = 0;
  queryText               = '';
  segment                 = 'all';
  excludeTracks: any      = [];
  shownSessions: any      = [];
  products: any           = [];
  products_cats: any      = [];
  all_products_cats: any  = [];
  groups: any             = [];
  confDate: string;
  cat_f:any               = [];
  cat_s:any               = [];
  cat_t:any               = [];
  cat_frth:any            = [];
  mProductsEnquiry  : any = [];
  mEnquiry  : any         = [];
  id: 0;
  user: any;

  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides;

  SwipedTabsIndicator: any = null;
  pages: any = [];


  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public confData: ConferenceData,
    public productsData: ProductsData,
    public userData: UserData,
    public http: Http,
  ) {
    app.setTitle('Products');
  }

  ionViewDidLoad() {
    this.app.setTitle('Products');
    this.getProducts();
  }

  

  ionViewDidEnter() {
    this.SwipedTabsIndicator = document.getElementById("indicator");
  }

  selectTab(index) {
    console.log(index);
    
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (100 * index) + '%,0,0)';
    this.SwipedTabsSlider.slideTo(index, 500);
  }

  updateIndicatorPosition() {
    // this condition is to avoid passing to incorrect index
    if (this.SwipedTabsSlider.length() > this.SwipedTabsSlider.getActiveIndex()) {
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (this.SwipedTabsSlider.getActiveIndex() * 100) + '%,0,0)';
    }

  }

  animateIndicator($event) {
    if (this.SwipedTabsIndicator)
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d(' + (($event.progress * (this.SwipedTabsSlider.length() - 1)) * 100) + '%,0,0)';
  }
  

  getUserData() {
    
    this.userData.getUser().then((res: any) => {
        
        let _user = JSON.parse(res);
        this.user = _user;
       
        
      });
    
  }



      updateSchedule() {
        // Close any open sliding items when the schedule updates
        this.getProducts();
      }

      getProducts() {
      
        this.productsData.getProducts().subscribe((res:any)=> {
          this.products = res.data;
          this.pages    = res.cat;
          
        })
          
      }

      
      selectedCate(product:any){
        
        console.log(product);
        this.mEnquiry.push(product);

      }
      /*add single inquiry */
    addInquiry(product:any){

      this.mProductsEnquiry.push(product);

    }

    goToProductDetail(product: any) {
      this.navCtrl.push(SpeakerDetailPage, { productId: product.product_id, singleProduct: product });
    }
    /*send all inquiry in variables*/
    onSendInquiry(){
  

      /* post */


      var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({ headers: headers });

      let postParams = new URLSearchParams();
      postParams.append('action', 'ENQUIRY');
      postParams.append('post', JSON.stringify(this.mEnquiry));
      
      
      this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString(), options)
        .map(res => res.json())
        .subscribe(response => {
          
          if (response){
            if (response.status==200){
               this.navCtrl.setRoot(this.navCtrl.getActive().component);
               this.presentToast(response.message);
            }else{
              this.presentToast(response.message);
            }
          }else{
            this.presentToast('Something Went Wrong');
          }
          
    
        });      
      
	  /* post */


        
        
    }
    
    /*Progress Dialog */
    presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Sending Enquiry...",
      duration: 3000,
        dismissOnPageChange: true                

    });
    loader.present();
  }
    /* Toast */
    presentToast(message: string) {
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
    
    

  
      presentFilter() {
        let modal = this.modalCtrl.create(ScheduleFilterPage, this.excludeTracks);
        modal.present();

        modal.onWillDismiss((data: any[]) => {
          if (data) {
            this.excludeTracks = data;
            this.updateSchedule();
          }
        });

      }
    
    

  goToSessionDetail(sessionData: any) {
    // go to the session detail page
    // and pass in the session data

    this.navCtrl.push(SessionDetailPage, { sessionId: sessionData.id, name: sessionData.name });
  }
    
    

  addFavorite(slidingItem: ItemSliding, sessionData: any) {

    if (this.user.hasFavorite(sessionData.name)) {
      // woops, they already favorited it! What shall we do!?
      // prompt them to remove it
      this.removeFavorite(slidingItem, sessionData, 'Favorite already added');
    } else {
      // remember this session as a user favorite
      this.user.addFavorite(sessionData.name);

      // create an alert instance
      let alert = this.alertCtrl.create({
        title: 'Favorite Added',
        buttons: [{
          text: 'OK',
          handler: () => {
            // close the sliding item
            slidingItem.close();
          }
        }]
      });
      // now present the alert on top of all other content
      alert.present();
    }

  }
    
    

  removeFavorite(slidingItem: ItemSliding, sessionData: any, title: string) {
    let alert = this.alertCtrl.create({
      title: title,
      message: 'Would you like to remove this session from your favorites?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            // they clicked the cancel button, do not remove the session
            // close the sliding item and hide the option buttons
            slidingItem.close();
          }
        },
        {
          text: 'Remove',
          handler: () => {
            // they want to remove this session from their favorites
            this.user.removeFavorite(sessionData.name);
            this.updateSchedule();

            // close the sliding item and hide the option buttons
            slidingItem.close();
          }
        }
      ]
    });
    // now present the alert on top of all other content
    alert.present();
  }

  openSocial(network: string, fab: FabContainer) {
    let loading = this.loadingCtrl.create({
      content: `Posting to ${network}`,
      duration: (Math.random() * 1000) + 500
    });
    loading.onWillDismiss(() => {
      fab.close();
    });
    loading.present();
  }

  doRefresh(refresher: Refresher) {
    
      this.getProducts();
      setTimeout(() => {
        refresher.complete();
        const toast = this.toastCtrl.create({
          message: 'Items are updated.',
          duration: 3000
        });
        toast.present();
      }, 1000);
    
  }

  handleChange(product:any){

    this.userData.getUser().then((res: any) => {

      let _user = JSON.parse(res);
      this.user = _user;
      var pro = { "app_user_id": _user.id, "product_id": product.product_id };
      this.mEnquiry.push(pro);


    });

   
  }
    
    
}
