import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AlertController, NavController, ToastController, LoadingController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { SchedulePage } from '../../pages/schedule/schedule';

@Component({
  selector: 'page-user',
  templateUrl: 'support.html'
})
export class SupportPage {

  submitted: boolean = false;
  user: any ;
  supportMessage: string;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public http: Http,
    public userData: UserData,
    public loadingCtrl: LoadingController
  ) {

  }


  ngAfterViewInit() {
    this.getUserData();
  }


  ionViewDidEnter() {
    
  }


  /*Progress Dialog */
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 8000,
      dismissOnPageChange: true

    });
    loader.present();
  }
  getUserData(){
     this.userData.getUser().then((res: any) => {
       let _user = JSON.parse(res);
       this.user = _user;
     });
  }


  /* Toast */
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  submit(form: NgForm) {
    
    this.submitted = true;

    if (form.valid) {
      
      /* post */
     


      var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({ headers: headers });

      let postData = {
        app_user_id: this.user.id,
        message: this.supportMessage
      }

      let postParams = new URLSearchParams();

      postParams.append('action', 'CONTACT_US');
      postParams.append('post', JSON.stringify(postData));

      this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString(), options)

        .map(res => res.json())

        .subscribe(response => {

          if (response.status == 200) {

            this.navCtrl.push(SchedulePage);

          } else {

            console.log(response);

          }
          this.presentToast(response.message);

        });

      /* post */





    }
  }

}
