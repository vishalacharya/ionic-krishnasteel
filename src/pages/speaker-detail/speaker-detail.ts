import { Component } from '@angular/core';
import { ToastController,NavController, NavParams, LoadingController} from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ConferenceData } from '../../providers/conference-data';
import { ProductsData } from '../../providers/products-data';
import { UserData } from '../../providers/user-data';
import { SchedulePage } from '../../pages/schedule/schedule';

@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html',
  providers:[ProductsData]

})
export class SpeakerDetailPage {
  speaker: any;
  product: any;
  mProducts: any=[];
  enquiry: any;
  app_user_id:any;
  user: any;
  // private mProduct: Product;
  
  constructor(
    public dataProduct: ProductsData,
    public dataProvider: ConferenceData,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public userData: UserData,
    public http: Http,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController

  ){}

  ionViewWillEnter() {

    this.dataProduct.getProducts().subscribe((res: any) => {

      
      if (res.data) {
        
        for (const product of res.data) {
            if (product && product.product_id === this.navParams.data.productId) {
              this.product = product;
              break;
            }

          }
        }
    })
  }

  /*Progress Dialog */
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 8000,
      dismissOnPageChange: true

    });
    loader.present();
  }

  /* Toast */
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


  
  addInquiry(product:any) {
    /* post */
    
    this.presentLoading();
    this.userData.getUser().then((res:any)=>{

      let user = JSON.parse(res);
      var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({ headers: headers });

      let postData = {
        app_user_id: user.id,
        product_id: product.product_id
      }

      let postParams = new URLSearchParams();

      postParams.append('action', 'ENQUIRY_SINGLE');
      postParams.append('post', JSON.stringify(postData));

      this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString(), options)
        .map(res => res.json())
        .subscribe(response => {
          if (response.status == 200) {
            this.navCtrl.push(SchedulePage);
          } else {
            console.log(response);
            window.location.reload();
            
          }
          this.presentToast(response.message);


        });

          

    });

    

  }
  
  goToSessionDetail(session: any) {
    this.navCtrl.push('SessionDetailPage', { sessionId: session.id });
  }

}
