import { Component } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';

import { NgForm } from '@angular/forms';

import { ToastController,NavController,LoadingController  } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { SignupPage } from '../signup/signup';

import { HomePage } from '../home/home';


@Component({
  selector: 'page-user',
  templateUrl: 'login.html',
  providers: [UserData]
  
})
export class LoginPage {
  login: UserOptions = { username: '', password: '' };
  submitted = false;
  status :any;

  constructor(
      public navCtrl: NavController,
      public userData: UserData,
      public http: Http,
      private toastCtrl: ToastController,
      public loadingCtrl: LoadingController
    ) 
    { }

   
    /*Progress Dialog */
    presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 5000,
        dismissOnPageChange: true                

    });
    loader.present();
  }

  /* Toast */
    presentToast(message:string) {
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
    
    
  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      /* post */
      var headers = new Headers();
          
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded' );
          
      let options = new RequestOptions({ headers: headers });

      let postParams = new URLSearchParams();
      let postData={
        'email': this.login.username,
        'pass':this.login.password
      }
      postParams.append('action', 'login');
      postParams.append('post', JSON.stringify(postData));
      
      this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString() , options)
      .map(res => res.json())      
              .subscribe(response =>   {
                if (response!='undefined'){

                  if (response.status == 200) {
                    this.userData.onLogin(JSON.stringify(response.data[0]));
                    this.navCtrl.push(HomePage);
                    
                  } else {
                    console.log(response);
                    

                  }
                  
                }
                this.presentToast(response.message);
              
            });   
      /* post */
      
      
      
    }
  }

 
  onSignup() {
    this.navCtrl.push(SignupPage);
  }
}
