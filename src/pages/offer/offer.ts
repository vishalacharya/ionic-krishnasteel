import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { AlertController, App, List, ModalController, NavController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { Offer } from '../../interfaces/offer';
import { ConferenceData } from '../../providers/conference-data';
import { OfferData } from '../../providers/offer-data';
import { UserData } from '../../providers/user-data';
import { OfferDetailPage } from '../offer-detail/offer-detail';


@Component({
  
  selector: 'page-offer',
  templateUrl: 'offer.html'
  
  
})
export class OfferPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  @ViewChild('scheduleList', { read: List }) scheduleList: List;

  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks: any = [];
  shownSessions: any = [];
  offers: any = [];
  groups: any = [];
  confDate: string;
  private mOffers: Offer[]
  id: 0;


  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public confData: ConferenceData,
    public offerData: OfferData,
    public user: UserData,
    public http: Http,
  ) {}

  
  ionViewDidLoad() {
    this.app.setTitle('Offers');
    this.getOffers();
  }

  goToOfferDetail(offer: any) {
    this.navCtrl.push(OfferDetailPage, { offerId: offer.offer_id });
  }
  
  updateSchedule() {
    this.getOffers();
  }

  getOffers() {
    this.offerData.getOffers().subscribe((response: any)=> {
      this.offers = response.data;
      this.mOffers = response.data;
      console.log(this.mOffers);
      
      
    })
  }


  doRefresh(refresher: Refresher) {
    
    this.getOffers();
      setTimeout(() => {
        refresher.complete();
        const toast = this.toastCtrl.create({
          message: 'Items are updated.',
          duration: 3000
        });
        toast.present();
      }, 1000);
    
  }
    
    
}
