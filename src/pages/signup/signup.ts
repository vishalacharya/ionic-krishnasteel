import { Component } from '@angular/core';

import { NgForm } from '@angular/forms';

import { Http, Headers, RequestOptions } from '@angular/http';

import { ToastController,NavController, LoadingController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { UserOptions } from '../../interfaces/user-options';

import { TabsPage } from '../tabs-page/tabs-page';


@Component({
  selector: 'page-user',
  templateUrl: 'signup.html'
})
export class SignupPage {
  

      user: any = { 
      
        activ:'',
        address1:'',
        address2:'',
        city:'',
        created:'',
        email:'',
        fname:'',
        id:'',
        last_login:'',
        lname:'',
        mobile:'',
        modified:'',
        name:'',
        pass:'',
        pcode:'',
        phone:'',
        state:'',
        token:''
        
    };


  signup: UserOptions = { username: '', password: '' };
  submitted = false;

  constructor(
    public navCtrl: NavController,
    public userData: UserData,
    public toastCtrl: ToastController,
    public loadingController: LoadingController,
    public http: Http
    
  ) { }


  /*Progress Dialog */
  presentLoading() {
    let loader = this.loadingController.create({
      content: "Craeting Account...",
      duration: 8000,
      dismissOnPageChange: true

    });
    loader.present();
  }

  /* Toast */
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {

      /* post */
      var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({ headers: headers });

      let postParams = new URLSearchParams();


      var postData = {

        id: this.user.id,
        activ: '1',
        address1: this.user.address1,
        address2: this.user.address1,
        city: this.user.city,
        created: this.user.created,
        email: this.user.email,
        fname: this.user.fname,
        last_login: this.user.last_login,
        lname: this.user.lname,
        mobile: this.user.mobile,
        modified: this.user.modified,
        name: this.user.name,
        pass: this.user.pass,
        pcode: this.user.pcode,
        phone: this.user.mobile,
        state: this.user.state,
        token: this.user.token

      }
      
     postParams.append('action','REGISTARTION');
     postParams.append('post', JSON.stringify(postData));

      
    /* rest call  */

     this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString(), options)
        .map((res=>res.json()))
        .subscribe(response => {
     
          if (response.status == 200) {

            postData['id'] = response.data.id;
            this.userData.onLogin(JSON.stringify(postData));
            this.navCtrl.push(TabsPage);
            
          }
          this.presentToast(response.message);

       });
    /* rest call   */
    
     

    }
  }
}
