import { Component } from '@angular/core';
import { NavController, NavParams, App} from 'ionic-angular';

import { ConferenceData } from '../../providers/conference-data';
import { OfferData } from '../../providers/offer-data';

@Component({
  selector: 'page-offer-detail',
  templateUrl: 'offer-detail.html'
  

})
export class OfferDetailPage {
  
  offer: any;

  constructor(public app: App,public dataOffer: OfferData,public dataProvider: ConferenceData, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
    this.app.setTitle('Offer');
    this.dataOffer.getOffers().subscribe((res: any) => {


      if (res) {
        
        console.log(res.data);
        for (const offer of res.data) {

          console.log(offer.offer_id);
          console.log(this.navParams.data);
          if (offer && offer.offer_id === this.navParams.data.offerId) {
            this.offer = offer;
            console.log(this.offer);
            
            break;
          }

        }
      }
    });
  }


  ionViewWillEnter() {

   
  }
}
