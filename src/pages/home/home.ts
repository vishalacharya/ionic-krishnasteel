import { Component } from '@angular/core';


import { 
  App,
  NavController, 
  ActionSheet,
  ActionSheetController,
  ActionSheetOptions,
  Config
  
 } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AccountPage } from '../account/account';
import { AboutPage } from '../about/about';
import { SchedulePage } from '../schedule/schedule';
import { SupportPage } from '../support/support';
import { OfferPage } from '../offer/offer';
import { SpeakerListPage } from '../speaker-list/speaker-list';
import { MapPage } from '../map/map';
import { ProductsData } from '../../providers/products-data';


// TODO remove
export interface ActionSheetButton {
  text?: string;
  role?: string;
  icon?: string;
  cssClass?: string;
  handler?: () => boolean | void;
};


@Component({
  
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ProductsData]

})

export class HomePage {

  actionSheet: ActionSheet;
  sliders:any=[];
  products:any=[];
 

  constructor(
    public app: App,
    public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public inAppBrowser: InAppBrowser,
    public productsData: ProductsData,
    public config: Config

 ) {}

  goToPage(pageID:any){

    switch (pageID) {
      
      case 'Enquiry':
        
        this.navCtrl.push(SupportPage);
        console.log(pageID);
        
        break;
      case 'Contact':
        
        this.navCtrl.push(SpeakerListPage);
        console.log(pageID);
        break;
        
      case 'Profile':
        
        this.navCtrl.push(AccountPage);
        console.log(pageID);
        break;  
    
      case 'Products':

        this.navCtrl.push(SchedulePage);
        console.log(pageID);
        break;

      case 'Offers':
        
        this.navCtrl.push(OfferPage);
        console.log(pageID);
        break;
        
      case 'About':
        
        this.navCtrl.push(AboutPage);
        console.log(pageID);
        break;

      case 'Map':

        this.navCtrl.push(MapPage);
        console.log(pageID);
        break;

      default:
        break;
    }
  }
 
  ionViewDidLoad() {
    this.app.setTitle('Products');
    this.getProducts();
  }

  getProducts() {

    this.productsData.getProducts().subscribe((res: any) => {
      
      for (let index = 0; index < 5; index++) {
        this.products.push(res.data[index]);    
      }
      console.log(this.products);
      
   
    })

  }
  onPhoneClick(){

    let mode = this.config.get('mode');
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Contact Mr. Rajesh Modi',
      buttons: [
        {
          text: `Call ( 9099977468 )`,
          icon: mode !== 'ios' ? 'call' : null,
          handler: () => {
            window.open('tel:+9099977468');
          }
        } as ActionSheetButton
      ]
    } as ActionSheetOptions);

    actionSheet.present();
  }

  onMailClick(){
      let mode = this.config.get('mode');
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Email us',
        buttons: [
          {
            text: `Email ( info@steelfurniture.co )`,
            icon: mode !== 'ios' ? 'mail' : null,
            handler: () => {
              window.open('mailto:info@steelfurniture.co');
            }
          } as ActionSheetButton,
        ]
      } as ActionSheetOptions);

      actionSheet.present();
  }

  onWebsiteClick(){

    // console.log(speaker);
    this.inAppBrowser.create(
      'www.steelfurniture.co',
      '_blank'
    );
    // window.open('www.steelfurniture.co', '_system');
  }

  
    
    
}
