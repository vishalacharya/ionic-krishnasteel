import { Component } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';

import { NgForm } from '@angular/forms';

import { ToastController,AlertController, NavController, LoadingController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';

import { User } from '../../interfaces/user';

import { TabsPage } from '../tabs-page/tabs-page';

import { SchedulePage } from '../../pages/schedule/schedule';


@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
  
})
export class AccountPage {
  
  user: User;
  username: string;
  activ: string;
  address1: string;
  address2: string;
  city: string;
  created: string;
  email: string;
  fname: string;
  id: string;
  last_login: string;
  lname: string;
  mobile: string;
  modified: string;
  name: string;
  pass: string;
  pcode: string;
  phone: string;
  state: string;
  token: string;
  
  submitted = false;


  constructor(private toastCtrl: ToastController,public alertCtrl: AlertController,  public userData: UserData, public loadingController: LoadingController, public http: Http, public navCtrl: NavController) {

    this.getUser();
  }



  ngAfterViewInit() {
    this.getUser();
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }
  /* Toast */
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        this.userData.setUsername(data.username);
        this.getUsername();
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  getUser() {
    this.userData.getUser().then((user) => {
      this.user = JSON.parse(user);
      this.fname    = this.user.fname;
      this.lname    = this.user.lname;
      this.address2 = this.user.address2;
      this.address1 = this.user.address1;
      this.email    = this.user.email;
      this.pass     = this.user.pass;
      this.mobile   = this.user.mobile;
      this.phone    = this.user.phone;
      
      console.log(this.user);
    });
  }

  changePassword() {
    console.log('Clicked to change password');
  }

  logout() {
    this.userData.logout();
    this.navCtrl.setRoot('LoginPage');
  }

  support() {
    this.navCtrl.push('SupportPage');
  }

  /*Progress Dialog */
  presentLoading() {
    let loader = this.loadingController.create({
      content: "Updating Account...",
      duration: 8000,
      dismissOnPageChange: true

    });
    loader.present();
  }
  /* modal */
  onUpdate(form: NgForm) {
    
    
    this.submitted = true;

    if (form.valid) {



      /* post */
      var headers = new Headers();

      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({ headers: headers });

      let postParams = new URLSearchParams();
      
      var postData = {
  
        activ: '1',
        address1: this.address1,
        address2: this.address1,
        city: this.user.city,
        created: this.user.created,
        email: this.email,
        fname: this.fname,
        id: this.user.id,
        last_login: this.user.last_login,
        lname: this.user.lname,
        mobile: this.mobile,
        modified: this.user.modified,
        name: this.user.name,
        pass: this.pass,
        pcode: this.user.pcode,
        phone: this.mobile,
        state: this.user.state,
        token: this.user.token

      }

      postParams.append('action', 'PROFILE_UPDATE');
      postParams.append('post', JSON.stringify(postData));
      
      //console.log(postParams);


      this.http.post("http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php", postParams.toString(), options)
        
       .map(res => res.json())
      
       .subscribe(response => {

          if (response.status == 200) {
            console.log(response);
            console.log(TabsPage);
              this.userData.onLogin(JSON.stringify(postData));
              this.navCtrl.push(TabsPage);
          }

          this.presentToast(response.message);
          

        });

           /* post */



    }
  }

  onBack() {
    this.navCtrl.push(SchedulePage);
  }
}
