import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Injectable()
export class UserData {
  _favorites: string[] = [];
  HAS_LOGGED_IN     = 'hasLoggedIn';
  HAS_SEEN_TUTORIAL = 'hasSeenTutorial';

  constructor(public events: Events,public storage: Storage) {}

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  };

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  };

  removeFavorite(sessionName: string): void {
    let index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  };

  /* Vishal acharya code */
  onLogin(userstr: string): void {
    this.setUser(userstr);
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.events.publish('user:login');

  };

  onSignup(user: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUser(user);
    this.events.publish('user:signup');
  };


  setUser(user: string): void {
    this.storage.set('user', user);
  };

  getUser(): Promise<string> {
    return this.storage.get('user').then((value) => {
      return value;
    });
  };



  onLogout(): void {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('user');
    this.events.publish('user:logout');
  };


  /* Vishal acharya code */

  login(
          username: string,
          userid: string, 
          useremail: string, 
          userphone: string, 
          usermobile: string, 
          userlname: string, 
          usercity: string, 
          userstate: string, 
          userpass: string, 
          useraddress1: string, 
          useraddress2: string): void {

    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.setUserId(userid);
    this.setUserEmail(useremail);
    this.setUserPhone(userphone);
    this.setUserMobile(usermobile);
    this.setUserLname(userlname);
    this.setUserCity(usercity);
    this.setUserState(userstate);
    this.setUserPass(userpass);
    this.setUserAddress1(useraddress1);
    this.setUserAddress2(useraddress2);
    this.events.publish('user:login');

 };

  signup(username: string): void {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(username);
    this.events.publish('user:signup');
  };



  logout(): void {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('username');
    this.events.publish('user:logout');
  };





  setUsername(username: string): void {
    this.storage.set('username', username);
  };

  getUsername(): Promise<string> {
    return this.storage.get('username').then((value) => {
      return value;
    });
  };

  setUserId(userid: string): void {
    this.storage.set('userid', userid);
  };

  getUserId(): Promise<string> {
    return this.storage.get('userid').then((value) => {
      return value;
    });
  };

  setUserEmail(useremail: string): void {
    this.storage.set('useremail', useremail);
  };

  getUserEmail(): Promise<string> {
    return this.storage.get('useremail').then((value) => {
      return value;
    });
  };


  setUserPhone(userphone: string): void {
    this.storage.set('userphone', userphone);
  };

  getUserPhone(): Promise<string> {
    return this.storage.get('userphone').then((value) => {
      return value;
    });
  };

  setUserLname(userlname: string): void {
    this.storage.set('userlname', userlname);
  };

  getUserLname(): Promise<string> {
    return this.storage.get('userlname').then((value) => {
      return value;
    });
  };
  
  
  setUserPass(userpass: string): void {
    this.storage.set('userpass', userpass);
  };

  getUserPass(): Promise<string> {
    return this.storage.get('userpass').then((value) => {
      return value;
    });
  };


  setUserMobile(usermobile: string): void {
    this.storage.set('usermobile', usermobile);
  };

  getUserMobile(): Promise<string> {
    return this.storage.get('usermobile').then((value) => {
      return value;
    });
  };

  setUserAddress1(useraddress1: string): void {
    this.storage.set('useraddress1', useraddress1);
  };

  getUserAddress1(): Promise<string> {
    return this.storage.get('useraddress1').then((value) => {
      return value;
    });
  };

  setUserAddress2(useraddress2: string): void {
    this.storage.set('useraddress2', useraddress2);
  };

  getUserAddress2(): Promise<string> {
    return this.storage.get('useraddress2').then((value) => {
      return value;
    });
  };

  setUserCity(usercity: string): void {
    this.storage.set('usercity', usercity);
  };

  getUserCity(): Promise<string> {
    return this.storage.get('usercity').then((value) => {
      return value;
    });
  };
  
  
  setUserState(userstate: string): void {
    this.storage.set('userstate', userstate);
  };

  getUserState(): Promise<string> {
    return this.storage.get('userstate').then((value) => {
      return value;
    });
  };

  setUserPcode(userpcode: string): void {
    this.storage.set('userpcode', userpcode);
  };

  getUserPcode(): Promise<string> {
    return this.storage.get('userpcode').then((value) => {
      return value;
    });
  };



  hasLoggedIn(): Promise<boolean> {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };

  checkHasSeenTutorial(): Promise<string> {
    return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
      return value;
    });
  };
}
