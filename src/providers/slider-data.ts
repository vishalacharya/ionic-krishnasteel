import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Injectable()
export class SliderData {
    data: any;
    private apiUrl = 'http://lakshyamarketing.com/krishnasteel/client/api/v1/restapi.php?action=slider';

    constructor(public http: Http) { }

    getProducts(): any {

        return this.http.get(this.apiUrl).map(this.onSuccess, this.onFailure).catch(this.onError)

    }

    private onSuccess(data: any) {

        this.data = data.json();
        return this.data;

    }

    private onFailure(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


    private onError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const err = error || '';
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }




}
