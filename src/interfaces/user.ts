export interface User{

    activ:string,
    address1:string,
    address2:string,
    city:string,
    created:string,
    email:string,
    fname:string,
    id:string,
    last_login:string,
    lname:string,
    mobile:string,
    modified:string,
    name:string,
    pass:string,
    pcode:string,
    phone:string,
    state:string,
    token:string

}