export interface Product {

    product_id     : string,
    product_cat_id : string,
    fire_id        : string,
    uid            : string,
    name           : string,
    created        : string,
    modified       : string,
    desc           : string,
    img_1          : string,
    img_2          : string,
    img_3          : string,
    img_4          : string,
    img_5          : string

}
